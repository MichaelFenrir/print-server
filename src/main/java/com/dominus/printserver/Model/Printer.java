package com.dominus.printserver.Model;

import com.fasterxml.jackson.databind.util.JSONPObject;
import net.minidev.json.JSONObject;

import javax.print.*;
import javax.print.DocFlavor.CHAR_ARRAY;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static javax.print.DocFlavor.CHAR_ARRAY.TEXT_PLAIN;

public class Printer {
    private Integer impressoraId;
    private PrintService printService;

    public Printer() {
    }

    public Printer(Integer impressoraId) {
        this.impressoraId = impressoraId;
        setPrinter(impressoraId);
    }

    public ArrayList<String> getPrinters() {
        ArrayList<String> listPrint = new ArrayList<>();
        PrintService printers[] = PrintServiceLookup.lookupPrintServices(null,null);
        for (PrintService printer: printers) {
            listPrint.add(printer.getName());
        }
        return listPrint;
    }

    public PrintService getPrinter(){
        return this.printService;
    }

    public void setPrinter(Integer impressoraId) {
        this.printService = PrintServiceLookup.lookupPrintServices(null, null)[impressoraId];
    }

    public void print(String s) throws IOException, PrintException {
        InputStream texto = new ByteArrayInputStream(s.getBytes("UTF8"));
        PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        set.add(new Copies(1));
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc doc = new SimpleDoc(texto, flavor, null);
        DocPrintJob printJob = this.printService.createPrintJob();
        printJob.print(doc, null);
        texto.close();
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("printer", this.printService.getName());

        return json;
    }
}
