package com.dominus.printserver.Model;

import jssc.SerialPort;
import jssc.SerialPortException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;

public class SerialPrinter extends Printer {
    @Autowired
    SerialPort serialPort;
    public SerialPrinter() {
        super();
        serialPort = new SerialPort("COM5");

        try {
            System.out.println("Port opened: " + serialPort.openPort());
            //System.out.println("Params setted: " + serialPort.setParams(9600, 8, 1, 0));
            System.out.println("\"Hello World!!!\" successfully writen to port: " + serialPort.writeBytes("Hello World!!!".getBytes()));
            System.out.println("Port closed: " + serialPort.closePort());
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }


}
