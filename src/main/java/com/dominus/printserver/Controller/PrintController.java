package com.dominus.printserver.Controller;

import com.dominus.printserver.Model.Printer;
import com.dominus.printserver.Model.SerialPrinter;
import org.springframework.web.bind.annotation.*;

import javax.print.PrintException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@RestController
@CrossOrigin
public class PrintController {
    @RequestMapping(method = RequestMethod.GET, value = "/print")
    public ArrayList<String> getPrinters() {
        Printer printer = new Printer();
        return printer.getPrinters();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/print/{impressoraId}")
    public String print(@PathVariable("impressoraId") Integer impressoraId) {
        Printer printer = new Printer(impressoraId);
        try {
            printer.print("TESTE de TESTE");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (PrintException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return printer.toJson().toJSONString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/print/com")
    public void printSerial(){
        SerialPrinter print= new SerialPrinter();
    }
}
